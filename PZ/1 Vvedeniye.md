# 1 Введение

Существует множество различных таск-менеджеров, предназначенных как для больших команд, так и для персонального использования. Однако большинство из них рассчитаны большие долгоживущие проекты и постоянные команды. Ввиду этого они обладают значительным спектром функционала, но также становятся медленными, перегруженными и сложными в освоении
