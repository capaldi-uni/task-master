package com.nikadmin.taskmaster;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Master {
    private static Master _instance;

    //all user records
    private Map<Long, Project> _projects;
    private Map<Long, Task> _tasks;
    private Map<Long, Subtask> _subtasks;
    private Map<Long, Note> _notes;

    //trash bin
    private Map<Long, Project> r_projects;
    private Map<Long, Task> r_tasks;
    private Map<Long, Subtask> r_subtasks;
    private Map<Long, Note> r_notes;

    private long idFree;

    //singleton goes brrr
    private Master() {
        idFree = 1;

        _projects = new HashMap<Long, Project>();
        _tasks = new HashMap<Long, Task>();
        _subtasks = new HashMap<Long, Subtask>();
        _notes = new HashMap<Long, Note>();

        r_projects = new HashMap<Long, Project>();
        r_tasks = new HashMap<Long, Task>();
        r_subtasks = new HashMap<Long, Subtask>();
        r_notes = new HashMap<Long, Note>();
    }
    public static Master instance() {
        if(_instance == null) {
            _instance = new Master();
        }
        return _instance;
    }

    //accessing items
    public Project project(Long id) {return _projects.get(id); }
    public Task task(Long id) { return _tasks.get(id); }
    public Subtask subtask(Long id) { return _subtasks.get(id); }
    public Note note(Long id) { return _notes.get(id); }

    //accessing removed items
    public Project projectR(Long id) {return r_projects.get(id); }
    public Task taskR(Long id) { return r_tasks.get(id); }
    public Subtask subtaskR(Long id) { return r_subtasks.get(id); }
    public Note noteR(Long id) { return r_notes.get(id); }

    //adding existing items ()
    private void addProject(Project project) { _projects.put(project.id(), project); }
    private void addTask(Task task) { _tasks.put(task.id(), task); }
    private void addSubtask(Subtask subtask) { _subtasks.put(subtask.id(), subtask); }
    private void addNote(Note note) { _notes.put(note.id(), note); }

    //adding new items
    //projects
    public Long addProject(Long id, String name) {
        Project project = new Project(idFree, name);
        addProject(project);
        idFree++;
        return project.id();
    }
    public Long addProject(Long id, String name, Date deadline) {
        Project project = new Project(idFree, name, deadline);
        addProject(project);
        idFree++;
        return project.id();
    }

    //tasks
    public Long addTask(String name) {
        Task task = new Task(idFree, name);
        addTask(task);
        idFree++;
        return task.id();
    }
    public Long addTask(String name, Date deadline) {
        Task task = new Task(idFree, name, deadline);
        addTask(task);
        idFree++;
        return task.id();
    }
    public Long addTask(String name, Long pid) {
        Task task = new Task(idFree, name, pid);
        addTask(task);
        idFree++;
        return task.id();
    }
    public Long addTask(String name, Date deadline, Long pid) {
        Task task = new Task(idFree, name, deadline, pid);
        addTask(task);
        idFree++;
        return task.id();
    }

    //subtasks
    public Long addSubtask(String name, Long task) {
        Subtask subtask = new Subtask(idFree, name, task);
        _subtasks.put(subtask.id(), subtask);
        idFree++;
        return subtask.id();
    }

    //notes
    public Long addNote(String name) {
        Note note = new Note(idFree, name);
        _notes.put(note.id(), note);
        idFree++;
        return note.id();
    }
    public Long addNote(String name, String text) {
        Note note = new Note(idFree, name, text);
        _notes.put(note.id(), note);
        idFree++;
        return note.id();
    }

    //removing items
    public void removeProject(Long id) { r_projects.put(id, _projects.remove(id)); }
    public void removeTask(Long id) { r_tasks.put(id, _tasks.remove(id)); }
    public void removeSubtask(Long id) { r_subtasks.put(id, _subtasks.remove(id)); }
    public void removeNote(Long id) { r_notes.put(id, _notes.remove(id)); }

    public List<Long> projects() { return new ArrayList<>(_projects.keySet()); }
    public List<Long> tasks() { return new ArrayList<>(_tasks.keySet()); }
    public List<Long> subtasks() { return new ArrayList<>(_subtasks.keySet()); }
    public List<Long> notes() { return new ArrayList<>(_notes.keySet()); }

    public List<Long> projectsR() { return new ArrayList<>(r_projects.keySet()); }
    public List<Long> tasksR() { return new ArrayList<>(r_tasks.keySet()); }
    public List<Long> subtasksR() { return new ArrayList<>(r_subtasks.keySet()); }
    public List<Long> notesR() { return new ArrayList<>(r_notes.keySet()); }

}
