package com.nikadmin.taskmaster;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* task itself
 * can be checked, have deadline and contain subtasks
 * can be standalone or in project(project id as last argument)
 */
public class Task extends Item {
    private boolean _checked;
    private List<Long> _subtasks;
    private Date _deadline;
    private boolean _isDeadline;
    private Long _project;



    public Task(Long id, String name) { super(id, name); allocateLists();}
    public Task(Long id, String name, Date deadline) {
        super(id, name);
        _deadline = deadline;
        _isDeadline = true;
        allocateLists();
    }
    public Task(Long id, String name, Long pid) { super(id, name); _project = pid; allocateLists();}
    public Task(Long id, String name, Date deadline, Long pid) {
        super(id, name);
        _deadline = deadline;
        _isDeadline = true;
        _project = pid;
        allocateLists();
    }

    private void allocateLists() { _subtasks = new ArrayList<>(); }

    //check management
    public boolean isChecked() { return _checked; }
    public void setChecked(boolean _checked) { this._checked = _checked; }
    public void check() { setChecked(true); }
    public void uncheck() { setChecked(false); }

    //subtasks management
    public List<Long> subtasks() { return _subtasks; }
    public void addSubtask(Long id) { _subtasks.add(id); }
    public void removeSubtask(int index) { _subtasks.remove(index); }

    //deadline management
    public boolean isDeadline() { return _isDeadline; }
    public Date deadline() { return _deadline; }
    public void setDeadline(Date deadline) { _deadline = deadline; _isDeadline = true; }
    public void resetDeadline() { _isDeadline = false; }

    //parent project
    public Long project() { return _project; }
    public void setProject(Long id) { _project = id; }

    public boolean isFailed() {
        Date current = new Date();
        return _deadline.after(current);
    }
}
