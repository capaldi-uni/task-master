package com.nikadmin.taskmaster;

/* base for all other items
 * saves 12-16 lines. roughly
 */
public abstract class Item {
    private String _name;
    private Long _id;

    Item(Long id) { _id = id; }
    Item(Long id, String name) { _id = id; _name = name; }

    public String name() { return _name; }
    public void setName(String name) { _name = name; }
    public Long id() { return _id; }
    public void setId(Long id) { _id = id; }
}
