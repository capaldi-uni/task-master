package com.nikadmin.taskmaster;

/* just note
 * can be inside project
 */
public class Note extends Item {
    private String _text;

    public Note(long id, String name) { super(id, name); }
    public Note(long id, String name, String text) { super(id, name); _text = text; }

    public String text() { return _text; }
    public void setText(String text) { _text = text; }
}
