package com.nikadmin.taskmaster;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        final EditText inputText = findViewById(R.id.inputText);
        final TextView contentText = findViewById(R.id.contentText);
        ImageButton addButton = findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String content = contentText.getText().toString();
                content = content.concat("\n");
                content = content.concat(inputText.getText().toString());
                contentText.setText(content);
            }
        });

    }
}
