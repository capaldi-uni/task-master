package com.nikadmin.taskmaster;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* task and note aggregator
 * can have deadline
 * finished if all tasks checked
 */
public class Project extends Item {
    private List<Long> _tasks;
    private List<Long> _notes;

    private Date _deadline;
    private boolean _isDeadline;

    public Project(Long id, String name) { super(id, name); allocateLists();}
    public Project(Long id, String name, Date deadline) {
        super(id, name);
        _deadline = deadline;
        _isDeadline = true;
        allocateLists();
    }

    private void allocateLists() { _tasks = new ArrayList<>(); _notes = new ArrayList<>(); }

    //task management
    public List<Long> tasks() { return _tasks; }
    public void addTask(Long id) { _tasks.add(id); }
    public void removeTask(int index) { _tasks.remove(index); }

    //note management
    public List<Long> notes() { return _notes; }
    public void addNote(Long id) { _notes.add(id); }
    public void removeNote(int index) { _notes.remove(index); }

    //deadline management
    public boolean isDeadline() { return _isDeadline; }
    public Date deadline() { return _deadline; }
    public void setDeadline(Date deadline) { _deadline = deadline; _isDeadline = true; }
    public void resetDeadline() { _isDeadline = false; }

    public boolean isFinished() {
        boolean f = true;

        for(Long key: _tasks) {
           if(!Master.instance().task(key).isChecked())
               f = false;
        }

        return f;
    }
    public float getProgress() {
        int count = _tasks.size();
        int completed = 0;

        for(Long key: _tasks) {
            if(Master.instance().task(key).isChecked())
                completed++;
        }

        return (float)completed / count;
    }

}
